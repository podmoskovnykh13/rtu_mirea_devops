import sys

def char_to_upper(char):
    if 'a' <= char <= 'z':
        return chr(ord(char) - ord('a') + ord('A'))
    return char

if len(sys.argv) == 2:
    input_char = sys.argv[1][0]
    upper_char = char_to_upper(input_char)

    print(f"Исходный символ: {input_char}")
    print(f"Преобразованный символ: {upper_char}")
else:
    print("Использование: python main.py <символ>")