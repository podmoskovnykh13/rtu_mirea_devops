import psycopg2
import time
import pandas as pd

time.sleep(20)

conn = psycopg2.connect(
    dbname="root",
    user="root",
    password="root",
    host="172.19.0.2",
    port="5432"
)

cursor = conn.cursor()

query = """
    SELECT s.name || ' ' || s.surname AS "ФИО студента"
    FROM students s
    JOIN record_book rb ON s.id = rb.student_id
    WHERE rb.teacher = 'Сидорова О.М.';
"""

cursor.execute(query)
rows = cursor.fetchall()

with open('result.txt', 'w') as file:
    for row in rows:
        file.write(f"{row[0]}\n")

cursor.close()
conn.close()
